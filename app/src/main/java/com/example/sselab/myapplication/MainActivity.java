package com.example.sselab.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickButton(View v) {
        if (v == findViewById(R.id.btnSave)) {
            EditText edit = (EditText) findViewById(R.id.editText);
            String input = edit.getText().toString();

            String msg_str;
            if (input.length() > 50)
                msg_str = "저장할 수 없습니다.";
            else
                msg_str = input + "가 SAVE 되었습니다.";

            Toast msg = Toast.makeText(
                    MainActivity.this,
                    msg_str,
                    Toast.LENGTH_SHORT);
            msg.show();

        } else if (v == findViewById(R.id.btnCancel)) {
            EditText edit = (EditText) findViewById(R.id.editText);
            edit.setText("");

        } else if (v == findViewById(R.id.btnExit)) {
            finish();
        }
    }
}
